# Find-a-scan

## TLDR;

* Inspired by a friend's request to suggest a solution for managing fax scans in a large organization
* Created sample serverless application architecture with a minimal [hosting cost](https://docs.google.com/spreadsheets/d/1Ywuz7k7saM7ue-YwXyDZLRirH0g1d7l0KYS71dQ7vuI/edit?usp=sharing)
* Files are ploaded to S3 bucket using aws-cli i.e. no custom code
* File path is indexed into ElasticSearch to facilitate search inside the bucket content, something that S3 is not great at
* In the design of components managed to avoid any single points of failure to support an enterprise grade SLA's
* Strict confidentiality of the information on the wire and at rest: encryption configurable for S3 bucket and for Elastic Search
* Protected access to Kibana/ElasticSearch and access to S3 bucket content with Cognito managed credentials
* Conginto is AWS identity management service for public users with support for federated identities such as Microsoft Active Direcotry SAML and Google oAuth
* Reduced number of secrets required, whether embedded in code or stored in environment variables to minimize security vulnerabilities
* (Not implemented yet) sent uploaded images through OCR to improve searchability of the image

## Overview of the solution
![](./media/idfood-find-a-scan.jpg)

## Components

* [Lambda Function](https://bitbucket.org/vk-idfood/find-a-scan-s3-handler) triggered whenever a file is uploaded/updated to S3 bucket. The repository is configured for CI using Bitbucket pipelines. Configuration for the function is stored in an environment variable and contain no secrets.
* S3 Bucket stores uploaded files with server-side encryption enabled
* A sample setup for pushing files to the S3 bucket using aws-cli: https://bitbucket.org/vk-idfood/s3-awscli-sync/src/master/
* Another S3 Bucket hosts a [static web site](https://bitbucket.org/vk-idfood/find-a-scan-s3-cognito-proxy/src/master/) to provide secure access to S3 bucket files protected by Cognito Identity Pool
* Cognito User Pool to manage end-user credentials
* Cognito Identity Pool to protect access to ElasticSearch, Kibana, and to S3 content
* ElasticSearch Service to index files meta-data
* Kibana to provide a searchable user-interface with a link to display/download file content
* [Terraform and aws-cli](https://bitbucket.org/vk-idfood/find-a-scan-devops) to provide a completely automated setup for the solution

### Post Automated Deployment Steps

* A first image needs to be uploaded to the S3 storage bucket to index first document into ElasticSearch
* You can use AWS Web UI, aws cli s3 sync process, WinScp, or any other method for uploading the images to the bucket
* After first image has been uploaded to S3 bucket and indexed into the Elasticsearch
* Kibana index pattern needs to be configured: https://www.elastic.co/guide/en/kibana/current/index-patterns.html
![](./media/kibana-index-pattern.png)
* `key` field formatting needs to be changed to use `Url` rather than default `String` by selecting edit button far right to the field name
* In the `Url template` paste the template printed out from the deployment, e.g.:
`S3 Proxy Url for Kibana configuration, field 'key': https://idfood-dev-findascan-s3bucketproxy.s3.amazonaws.com/index.html?s3_key={{value}}`
![](./media/kibana-key-url-template.png)
* The `Discover` now should display the results where you can choose to select all/some fields from the indexed document:
![](./media/kibana-list.png)
* There is a Lucene syntax search capabilities:
![](./media/kibana-search.png)
* And selecting `key` url will open a new page with image content:
![](./media/s3-image.png)

